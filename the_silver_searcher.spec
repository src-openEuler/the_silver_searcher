Name:           the_silver_searcher
Version:        2.2.0
Release:        2
Summary:        Super-fast text searching tool (ag)
Group:          Applications/Text
License:        ASL 2.0 and BSD
URL:            https://github.com/ggreer/the_silver_searcher
Source:         https://github.com/ggreer/the_silver_searcher/archive/%{version}/the_silver_searcher-%{version}.tar.gz
# https://github.com/ggreer/the_silver_searcher/compare/2.2.0...5a1c8d83ba.patch
Patch0:         5a1c8d83ba.patch 
# https://github.com/ggreer/the_silver_searcher/pull/1145
Patch1:         0001-update-zsh-completion-for-new-options.patch
# https://github.com/ggreer/the_silver_searcher/pull/1410
Patch2:         0002-Install-shell-completion-files-to-correct-locations.patch
# https://github.com/aswild/the_silver_searcher/commit/9d6fc437f7ce2293eedc660d23b930d8b7ef6c43
Patch3:         Add-support-for-libpcre2.patch

BuildRequires:  autoconf gcc uthash-devel
BuildRequires:  automake make
BuildRequires:  pcre2-devel
BuildRequires:  xz-devel
BuildRequires:  zlib-devel

%description
The Silver Searcher is a code searching tool similar to ack,
with a focus on speed.

%prep
%autosetup -n %{name}-%{version} -p1
rm src/uthash.h
sed -e '/ag_SOURCES/ s/ src\/uthash.h//' -i Makefile.am

%build
aclocal
autoconf
autoheader
automake --add-missing
%configure --disable-silent-rules --with-pcre2
%make_build

%install
%make_install

%files
%license LICENSE            
%doc README.md
%{_bindir}/ag
%{_mandir}/man1/ag.1*
%{_datadir}/bash-completion/completions/ag            
%{_datadir}/zsh/site-functions/_ag

%changelog
* Thu Mar 14 2024 yaoxin <yao_xin001@hoperun.com> - 2.2.0-2
- Add support for libpcre2

* Sat Sep 16 2023 liyanan <thistleslyn@163.com> - 2.2.0-1
- Update to 2.2.0

* Tue Dec 13 2022 wangkai <wangkai385@h-partners.com> - 2.1.0-4
- Add make for buildrequires

* Sat 7 Aug 2021 zhangtao <zhangtao221@huawei.com> - 2.1.0-3
- fix gcc upgrade causes compilation failure

* Tue 27 Jul 2021 sunguoshuai <sunguoshuai@huawei.com> - 2.1.0-2
- Add gcc for buildrequires

* Wed Sep 16 2020 Li Chao <clouds@isrc.iscas.ac.cn> - 2.1.0-1
- First the_silver_searcher package
